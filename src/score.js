/**
 * @module Business logic for calculating scores of sensors.
**/
// TODO - write tests

const { getMean, getStandardDeviation } = require('./math');
const { HUMIDITY_SCORES, THERMOMETER_SCORES, SENSOR_TYPES } = require('./constants');

/**
 * Calculates the friendly score based on underlying mathemetical calculations.
 * @param  {number[]} temperatureReadings
 * @param  {number} referenceTemperature
 * @return {THERMOMETER_SCORES}
 */
function getThermometerScore(temperatureReadings, referenceTemperature) {
  const mean = getMean(temperatureReadings);
  const diff = Math.abs(referenceTemperature - mean);
  const stdDev = getStandardDeviation(temperatureReadings);

  if (diff <= 0.5 && stdDev < 3) {
    return THERMOMETER_SCORES.ULTRA_PRECISE;
  } else if (diff <= 0.5 && stdDev < 5) {
    return THERMOMETER_SCORES.VERY_PRECISE;
  }

  return THERMOMETER_SCORES.PRECISE;
}

/**
 * Calculates the friendly score based on underlying mathemetical calculations.
 * @param  {number[]} humidityReadings
 * @param  {number} referenceHumidity
 * @return {HUMIDITY_SCORES}
 */
function getHumidityScore(humidityReadings, referenceHumidity) {
  const allWithinRange = humidityReadings.every(reading => Math.abs(reading - referenceHumidity) <= 1.0);
  return allWithinRange ? HUMIDITY_SCORES.KEEP : HUMIDITY_SCORES.DISCARD;
}

/**
 * Calculates the friendly score for each of a set of sensors.
 * @param  {Object[]} sensorGroupings
 * @param  {{temperature: string, humidity: string}} reference
 * @return {{[string]: string}} Map of sensor name -> score
 */
function getQualityEvaluations(sensorGroupings, reference) {
  return sensorGroupings.map(g => {
    if (g.type === SENSOR_TYPES.THERMOMETER) {
      return {
        name: g.name,
        evaluation: getThermometerScore(g.readings, reference.temperature),
      };
    }

    if (g.type === SENSOR_TYPES.HUMIDITY) {
      return {
        name: g.name,
        evaluation: getHumidityScore(g.readings, reference.humidity),
      };
    }
  })
  .reduce((evaluations, e) => {
    return {
      ...evaluations,
      [e.name]: e.evaluation,
    };
  }, {});

  return null;
}

module.exports = {
  getQualityEvaluations,
}
