/**
 * @module Parse log file contents, making it easier to work with.
 */
// TODO - write tests

const { SENSOR_TYPES } = require("./constants");

/**
 * Turns raw log lines into sensor groupings, making them easier to process on
 * a per sensor level.
 * @param  {string[]} sensorLines Each sensor declaration or sensor reading
 * @return {{[string]: {name: string, type: string, readings: number[]}}} Sensor groups with embedded readings
 */
// TODO - add validation to ensure clean data
function parseSensorReadings(sensorLines) {
  let curSensorName;
  return sensorLines.reduce((acc, line) => {
    // skip empty lines
    if (!line) {
      return acc;
    }

    const [col1, col2] = line.split(" ");

    // Is it a 'sensor declaration' line?
    // TODO - with introduction of new devices, don't hardcode checks, instead
    // read from externally provided sensor types list
    if (col1 === SENSOR_TYPES.THERMOMETER || col1 === SENSOR_TYPES.HUMIDITY) {
      curSensorName = col2;
      return {
        ...acc,
        [curSensorName]: {
          name: col2,
          type: col1,
          readings: []
        }
      };
    }

    // Is it a 'reading' line?
    // TODO - add a check here for a valid date. probably use moment / luxon
    // or simply a regex since format is known.
    if (col1) {
      const sensorEntry = acc[curSensorName];
      // console.log('acc', acc);
      return {
        ...acc,
        [curSensorName]: {
          ...sensorEntry,
          readings: [...sensorEntry.readings, parseFloat(col2)]
        }
      };
    }

    // Skip line, didn't match something we expect.
    return acc;
  }, {});
}

/**
 * Extracts reference data from single log line, must be a reference line.
 * @param  {string} referenceLine raw reference line
 * @return {{temperature: number, humidity: number}} reference data
 */
function parseReferenceLine(referenceLine) {
  const [signal, temperature, humidity] = referenceLine.split(" ");
  if (signal !== "reference") {
    throw new Error("Invalid file format. Missing reference line");
    // TODO - add validation for temperature and humidity settings
  }

  return {
    temperature,
    humidity
  };
}

module.exports = {
  parseSensorReadings,
  parseReferenceLine
};
