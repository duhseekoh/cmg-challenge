const fs = require('fs');
const { evaluateLogFile } = require('./index');

describe('when given a log with multiple sensors data', () => {
  let stringLog;
  beforeEach(() => {
    stringLog = fs.readFileSync('./mock/mock-logs-1.log').toString();
  });


  it('should classify temp as precise for precise log data', () => {
    const evaluation = evaluateLogFile(stringLog);
    expect(evaluation['temp-1']).toEqual('precise');
  });

  it('should classify temp as ultra precise for ultra precise log data', () => {
    const evaluation = evaluateLogFile(stringLog);
    expect(evaluation['temp-2']).toEqual('ultra precise');
  });

  it('should classify humidity sensor as keepable for good humidity data', () => {
    const evaluation = evaluateLogFile(stringLog);
    expect(evaluation['hum-1']).toEqual('keep');
  });

  it('should classify humidity sensor as discardable for bad humidity data', () => {
    const evaluation = evaluateLogFile(stringLog);
    expect(evaluation['hum-2']).toEqual('discard');
  });

  // TODO - write more tests, possibly with randomly generated data
});
