const { parseReferenceLine, parseSensorReadings } = require('./parse');
const { getQualityEvaluations } = require('./score');

/**
 * Processes a log of sensor data to determine quality of the devices tested.
 * @param  {string} fileContents log file contents
 * @return {{[string]: string}} Map of sensor name -> score
 */
function evaluateLogFile(fileContents) {
  // Make the contents iterable
  const lines = fileContents.split('\n');
  console.log('lines', lines); // TODO - replace console.logs with a logger

  // We always know reference line is first
  const reference = parseReferenceLine(lines[0]);
  console.log('reference', reference);

  // Rest of file are sensor declarations and readings, group by those
  const readingsBySensor = parseSensorReadings(lines.slice(1));
  console.log('readingsBySensor', readingsBySensor);

  // Process each sensors data
  const qualityEvaluations = getQualityEvaluations(Object.values(readingsBySensor), reference);
  console.log('qualityEvaluations', qualityEvaluations);

  return qualityEvaluations;
};

module.exports = {
  evaluateLogFile,
};
