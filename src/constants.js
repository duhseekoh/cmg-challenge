/** 
 * @module App level constants
**/

const SENSOR_TYPES = {
  THERMOMETER: 'thermometer',
  HUMIDITY: 'humidity',
};

const THERMOMETER_SCORES = {
  ULTRA_PRECISE: 'ultra precise',
  VERY_PRECISE: 'very precise',
  PRECISE: 'precise',
};

const HUMIDITY_SCORES = {
  KEEP: 'keep',
  DISCARD: 'discard',
};

module.exports = {
  SENSOR_TYPES,
  THERMOMETER_SCORES,
  HUMIDITY_SCORES,
};
