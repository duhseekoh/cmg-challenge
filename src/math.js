// TODO - would likely pull in a math library to do anything further than this

/**
 * Gets the average of a list of numbers.
 * @param  {number[]} values
 * @return {number} the mean
 */
function getMean(values) {
  const sum = values.reduce((sum, t) => t + sum, 0);
  return sum / values.length;
}

/**
 * Gets the standard deviation of a list of numbers.
 * @param  {numnber[]} values
 * @return {number} standard deviation
 */
function getStandardDeviation(values) {
  const mean = getMean(values);
  const squareDiffs = values.map(value => {
    var diff = value - mean;
    var sqrDiff = diff * diff;
    return sqrDiff;
  });
  const avgSquareDiff = getMean(squareDiffs);
  return Math.sqrt(avgSquareDiff);
}

module.exports = {
  getMean,
  getStandardDeviation,
};
