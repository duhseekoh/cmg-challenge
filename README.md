## Overview
[Dom DiCicco's](https://github.com/duhseekoh) solution for the 37Widgets log processing challenge.

## Setup
1. `yarn install`
2. `yarn test`

If you'd like to import `evaluateLogFile`, link this module locally, then access that function by importing the module.

## Considerations in my solution
- I would add Flow or Typescript if given more time. I didn't want to bring in a compiler just for this basic problem.
- Excuse some of the possibly malformed JSDoc. I'd rather have all those types defined in Flow/Typescript. But I wanted to get the point across that I was thinking about a type system.
- Assumed that the data set would be small enough that being able to parse the entirety of the file contents first would be ok. If it were a much larger dataset, I would most likely want to stream this data in (from a file or a streaming data solution like kinesis/kafka).
- I didn't add much validation but sprinkled a bit to get an idea of how that could be accomplished.
- If the rules were to grow more complex I would implement some type of rules engine to process each grouping, rather than implement functions for them. E.g. if there were a ton of different score possibilities and they overlapped in the calculations that ultimately determine each score.
- Also, if the number of sensors were to grow, I might come up with a solution that allows for sensor scoring calculations to be passed in via a configuration file.
- I'd imagine that most sensor log data for a test environment wouldn't come already grouped together. It might be best to have each log file line include the sensor type and name, so line order doesn't matter.
- Tests were written to verify the example outlined in the problem statement. I would add more tests and test each module and exported function from each module if I spent more time. I at least wanted to cover that base case though.
- TODOs are throughout the code and indicate areas where I would improve upon.
